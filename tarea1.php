<?php
/*
Plugin Name: Tarea 1
Plugin URI: http://www.yourpluginurlhere.com/
Version: 0.1
Author: Franchest
Description: Tarea con 15pts
*/

//Quitar el metabox: publish ------------------------------------------
/* add_action( 'admin_head', 'remMBS');
function remMBS(){
    //remove_meta_box y luego vá el componentes,id del post y el contexto
    remove_meta_box( 'submitdiv', 'proyecto', 'side' );
} */

//Para que los shortcodes funcionen tienen que ir en minuscula el nombre
add_shortcode('proyectos', 'verMensaje');
function verMensaje(){
    //Iterar para encontrar la lista de peliculas -------------------------
    echo '<h1>Lista de proyectos</h1><br>';
    $mypost = array( 'post_type' => 'proyecto', 'posts_per_page' => '10' );
    $loop = new WP_Query( $mypost );
    while ($loop->have_posts()) : $loop->the_post(); ?>
        <center>
            <input type="hidden" name="ss" value="<?php $postX=get_post(the_ID()); ?>">
            <p><strong>Nombre: </strong><?php echo $postX->post_title ?></p>
            <p><strong>Empresa: </strong><?php echo $postX->empresa ?></p>
            <p><strong>Link: </strong><?php echo $postX->url ?></p>
            <p><strong>Fecha: </strong><?php echo $postX->dateFec ?></p>
        <br></center>
        <?php endwhile;
}


//Para que los shortcodes funcionen tienen que ir en minuscula el nombre
add_shortcode('postInfo', 'verDatos');
function verDatos(){
    //Iterar para encontrar la lista de peliculas -------------------------
    echo '<h1>Lista de post</h1><br>';
    $mypost = array( 'post_type' => 'post', 'posts_per_page' => '10' );
    $loop = new WP_Query( $mypost );
    while ($loop->have_posts()) : $loop->the_post(); ?>
        <center>
            <input type="hidden" name="ss" value="<?php $postX=get_post(the_ID()); ?>">
            
            <script type="text/javascript">console.log("<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(the_ID()))[0] ?>")</script>
        <br></center>
        <?php endwhile;
}

//Agregar columnas al post type proyecto ----------------------------------
function agregar_columna_proyectos($columnas){
    $post_type = get_post_type();
    if ( $post_type == 'proyecto' ) {
        $columnas['title']='Nombre';
        $columnas['company']='Empresa';
        $columnas['url']='Link';
    }
    return $columnas;
}
add_filter( 'manage_posts_columns', 'agregar_columna_proyectos' );

//Agregar valor a las columnas -------------------------------------------
function fill_movies_posts_columns( $column_name, $post_id ) {
    $postX=get_post($post_id);
    switch( $column_name ):
        case 'url':
            echo $postX->url;
            break;
        case 'company':
            echo $postX->empresa;
            break;
    endswitch;
}
add_action( 'manage_proyecto_posts_custom_column', 'fill_movies_posts_columns', 2, 10 ); //Antes 10,2

//Bulk action para exportar a CSV ----------------------------------------
add_filter('bulk_actions-edit-proyecto', function($bulk_actions) {
	$bulk_actions['export-to-CSV'] = __('Exportar a CSV','txtdomain');	
    return $bulk_actions;
});
//Filtro para exportar CSV con los bulk actions -----------------------------
add_filter('handle_bulk_actions-edit-proyecto','actionBulk',10,3);
function actionBulk($redirect_to,$doaction,$post_id){
    if($doaction=='export-to-CSV'){
        printf('<div id="message" class="updated notice is-dismissable"><p>' . __('Publicaste %d posts.', 'txtdomain') . '</p></div>', $num_changed);
    }
    header('Content-Description: File Transfer');
    header('Content-Type: application/csv');
    header("Content-Disposition: attachment; filename=Proyectos.csv");
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    $handle = fopen('php://output', 'w');
    ob_clean(); // clean slate
    $campitos=['Nombre','Empresa','URL','Fecha'];
    fputcsv($handle,$campitos);
    foreach( $post_id as $postX ) {
        $postXS=get_post($postX);
        $valors=[$postXS->post_title,$postXS->empresa,$postXS->url,$postXS->dateFec];
        fputcsv($handle, $valors);
    }
    ob_flush();
    fclose($handle);
    die();
}

//Crear el post type ---------------------------------------------------------------
add_action( 'init', 'ctp_proyectos' );
function ctp_proyectos() {
	$labels = array(
	'name' => _x( 'Proyectos', 'post type general name' ), //Titulo del op.Menu
        'singular_name' => _x( 'Proyecto', 'post type singular name' ),
        'add_new' => _x( 'Añadir nuevo', 'book' ),
        'add_new_item' => __( 'Añadir nuevo Proyecto' ),
        'edit_item' => __( 'Editar Proyecto' ),
        'new_item' => __( 'Nuevo Proyecto' ),
        'view_item' => __( 'Ver Proyectos' ),
        'search_items' => __( 'Buscar Proyectos' ),
        'not_found' =>  __( 'No se han encontrado Proyectos' ),
        'not_found_in_trash' => __( 'No se han encontrado Proyectos en la papelera' ),
        'parent_item_colon' => ''
    );
    $args = array( 'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'show_in_rest' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 88, //Para settings
        'supports' => array( 'title')
    );
    register_post_type( 'proyecto', $args ); /* Registramos y a funcionar */

}

//Agregar datos adicionales -----------------------------------------------------------------------------------------
add_action( 'add_meta_boxes', 'proyectos_metabox' );
function proyectos_metabox(){
	add_meta_box( 'my-meta-box-id', 'Ficha de proyecto', 'proyectos_metabox_c', 'proyecto', 'normal', 'high' );
}
function proyectos_metabox_c($post){
	$values = get_post_custom( $post->ID );
	$Emp = isset( $values['empresa'] ) ? esc_attr( $values['empresa'][0] ) : '';
	$link = isset( $values['url'] ) ? esc_attr( $values['url'][0] ) : '';
    $fec = isset( $values['dateFec'] ) ? esc_attr( $values['dateFec'][0] ) : '';
    wp_nonce_field( 'my_meta_box_nonce', 'meta_box_nonce' );
    ?>
	<p>
		<label for="empresa">Empresa</label>
		<input type="text" name="empresa" id="empresa" value="<?php echo $Emp; ?>" />
    </p>
    <p>
		<label for="url">URL</label>
		<input type="text" name="url" id="url" value="<?php echo $link; ?>" />
    </p>
    <p>
		<label for="dateFec">Fecha:</label>
		<input type="date" name="dateFec" id="dateFec" value="<?php echo $fec; ?>" />
    </p>
    <a href="http://localhost/wordpress/wp-admin/edit.php?post_type=proyecto"><img src="http://aquaterra-soluciones.com/img/cms/VOLVER.png" style="padding-top:5px;padding-left:10px;width:130px;height:50px;"></a>
    <?php
}

//Agregar "Proyecto" al listado de post types ------------------------------------------------------------
add_action( 'pre_get_posts', 'agregar_proyecto_listado' );
function agregar_proyecto_listado( $query ) {
	if ( is_home() && $query->is_main_query() )
		$query->set( 'post_type', array( 'post', 'page', 'proyecto' ) );
	return $query;
}

//Guardar los datos adicionales ---------------------------------------------------------------------------
add_action( 'save_post', 'proyectos_metabox_save' );
function proyectos_metabox_save($post_id){
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'my_meta_box_nonce' ) ) return;
    if( !current_user_can( 'edit_post' ) ) return;
    $allowed = array(
        'a' => array( // on allow a tags  
            'href' => array() // and those anchors can only have href attribute  
        )
    );
    if( isset( $_POST['empresa'] ) )
        update_post_meta( $post_id, 'empresa', wp_kses( $_POST['empresa'], $allowed ) );
    if( isset( $_POST['url'] ) )
        update_post_meta( $post_id, 'url', wp_kses( $_POST['url'], $allowed ) );
    if( isset( $_POST['dateFec'] ) )
        update_post_meta( $post_id, 'dateFec', wp_kses( $_POST['dateFec'], $allowed ) );
}


//Para la tarea 2 -------------------------------------------------------------------------------------------
//Registrar los campos en rest
add_action( 'rest_api_init', function () {

    //Para proyectos -----------------------------------------------
    register_rest_field( 'proyecto', 'EMPRESA', array(
        'get_callback' => function( $post_arr ) {
            return get_post_meta( $post_arr['id'], 'empresa', true );
        },
    ) );
    register_rest_field( 'proyecto', 'URL', array(
        'get_callback' => function( $post_arr ) {
            return get_post_meta( $post_arr['id'], 'url', true );
        },
    ) );
    register_rest_field( 'proyecto', 'FECHA', array(
        'get_callback' => function( $post_arr ) {
            return get_post_meta( $post_arr['id'], 'dateFec', true );
        },
    ) );

    //Para posts -----------------------------------------------
    register_rest_field( 'post', 'NOMBRE', array(
        'get_callback' => function( $post_arr ) {
            return get_post($post_arr['id'])->post_title;
        },
    ) );
    register_rest_field( 'post', 'IMG', array(
        'get_callback' => function( $post_arr ) {
            return wp_get_attachment_image_src(get_post_thumbnail_id($post_arr['id']))[0];
        },
    ));
    register_rest_field( 'post', 'Cats', array(
        'get_callback' => function( $post_arr ) {
        $posttags = get_the_category($post_arr['id']);
        $tagP="";
        if ($posttags) {
            foreach($posttags as $tag) {
                $tagP=$tag->name.' | '.$tagP; 
            }
            $tagP=substr($tagP, 0,-3);
        }   
        return $tagP;
        },
    ) );
    register_rest_field( 'post', 'Tags', array(
        'get_callback' => function( $post_arr ) {
        $posttags = get_the_tags($post_arr['id']);
        $tagP="";
        if ($posttags) {
            foreach($posttags as $tag) {
                $tagP=$tag->name.' | '.$tagP; 
            }
            $tagP=substr($tagP, 0,-3);
        }   
        return $tagP;
        },
    )); 
    register_rest_field( 'post', 'Autor', array(
        'get_callback' => function( $post_arr ) {
            return get_the_author_meta('display_name', get_post($post_arr['id'])->post_author);
        },
    ));

} );

?>