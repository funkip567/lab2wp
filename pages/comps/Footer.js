const Footer = () => {
    return (
        <div style={{ position: "absolute", bottom: 0, width:"99%" }}>
            <p>© F R A N C H E S T. All rights reserved.</p>
        </div>
    );
}
  
  export default Footer;