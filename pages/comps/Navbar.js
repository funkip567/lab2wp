import Link from 'next/link';
const NavBar = () => {
    return(
        <ul className="navU">
            <li><Link href="/listaProyectos" as="/verProyectos"><a>Lista de proyectos</a></Link></li>
            <li><Link href="/listaPosts" as="/verPosts"><a>Lista de posts</a></Link></li>
            <li><Link href="/contact" as="/contacto"><a>Contacto</a></Link></li>
            <li><Link href="/aboutme" as="/devFRANCHEST"><a>Acerca de mi</a></Link></li>
            <li><Link href="/"><a>Inicio</a></Link></li>
        </ul>
    );
}
export default NavBar;