import Head from 'next/head';
import Link from 'next/link';
import NavBar from './comps/NavBar';
import Footer from './comps/Footer';
import { useRouter } from 'next/router';

const postDetails = ({details}) => {
    if(details=="N/A"){
        return(<p>No hay datos</p>);
    }
    return(
        <div>
            <Head>
                <title>Detalle de la entrada</title>
                <link rel="icon" href="http://www.classicgaming.cc/classics/pac-man/images/icons/pac-man.ico" />
            </Head>
            <NavBar />
            <br/><br/>
            <div className="divIniII">
                <h1>Detalles de la entrada</h1>
                <img src={details.IMG}/>
                <p>ID: {details.id}</p>
                <p>Nombre: {details.title['rendered']}</p>
                <p>Fecha: {details.date}</p>
                <p>Etiquetas: {details.Tags}</p>
                <p>Categorias: {details.Cats}</p>
                <p>Autor: {details.Autor}</p>
                <p>Slug: {details.slug}</p>
                <p>URL: {details.link}</p>
                <p>Tipo: {details.type}</p>
                <p>Estado: {details.status}</p>
                <p>Contenido: {details.content['rendered']}</p>
            </div>
            <Footer/>
        </div>
    )
};

 export default postDetails; 


 export async function getServerSideProps({query:{slug}}){
    if(slug!=null){
        const req = await fetch('http://localhost/wordpress/wp-json/wp/v2/posts/'+slug)
        const data = await req.json()
    return {
        props: {
            details: data},
        };
    }
    else{
        return("N/A");
    }
}