import Head from 'next/head';
import Link from 'next/link';
import NavBar from './comps/NavBar';
import Footer from './comps/Footer';
//import Sti from './components/test.module.css';

const listaPosts = ({posts}) => (
    <div>
        <Head>
            <title>Lista de posts</title>
            <link rel="icon" href="http://www.classicgaming.cc/classics/pac-man/images/icons/pac-man.ico" />
        </Head>
        <NavBar />
        <br/><br/>
        <div className="divIniIII">
            <h1>Lista de posts</h1>
            {posts.map((post)=>(<p class="listaPosts">{post.title['rendered']} <Link href={{pathname:"/postDetails",query:{'slug':post.id}}}><input type="submit" class="bttonPost" value="VER DATOS" /></Link></p>))}
        </div>
        <Footer />
    </div>
 );

 export async function getServerSideProps() {
    const req = await fetch('http://localhost/wordpress/wp-json/wp/v2/posts')
    const data = await req.json()

    return {
        props: {
            posts: data
        },
    };
}

 export default listaPosts;