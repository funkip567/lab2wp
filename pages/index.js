import Head from 'next/head';
import Link from 'next/link';
import NavBar from './comps/NavBar';
import Footer from './comps/Footer';
//import Sti from './components/test.module.css';

const Index = () => (
    <div>
        <Head>
            <title>Welcome to Tarea 2</title>
            <link rel="icon" href="http://www.classicgaming.cc/classics/pac-man/images/icons/pac-man.ico" />
        </Head>
        <NavBar />
        <div className="divIni">
            <h1>Bienvenido!</h1>
            <p>Para comenzar, seleccione un elemento del menu para continuar</p>
        </div>
        <Footer/>
    </div>
 );

 export default Index;