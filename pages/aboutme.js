import Head from 'next/head';
import Link from 'next/link';
import NavBar from './comps/NavBar';
import Footer from './comps/Footer';

const contact = () =>{
    return(
        <div>
            <Head>
                <title>Acerca de mi</title>
                <link rel="icon" href="http://www.classicgaming.cc/classics/pac-man/images/icons/pac-man.ico" />
            </Head>
            <NavBar/>
            <div className="divIniIII">
                <h1>Francisco Ponzoni</h1>
                <p>Estudiante de la carreara 'Tecnólogo en Informática' en la ciudad de Paysandú.</p>
                <p>Amante de la tecnología y el arte.</p>
                <p>#INFORMATICA #TEC #2021</p>
                <img className="divIMGIniIII" src="https://pbs.twimg.com/media/D-FlR2QWwAMZkCF?format=jpg&name=medium" />
            </div>
            <Footer/>
        </div>
    );
}
export default contact;