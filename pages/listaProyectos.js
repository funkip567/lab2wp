import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import NavBar from './comps/NavBar';
import Footer from './comps/Footer';

const listaProyectos = ({posts}) => (
    <div>
        <Head>
            <title>Lista de proyectos</title>
            <link rel="icon" href="http://www.classicgaming.cc/classics/pac-man/images/icons/pac-man.ico" />
        </Head>
        <NavBar />
        <br/><br/>
        <div className="divIniIII">
        <h1>Lista de proyectos</h1>
            <h2>Nombre | Empresa</h2>
            {posts.map((post)=>(<p className="listaPosts">{post.title['rendered']} | {post.EMPRESA} <Link href={{pathname:"/proyDetails",query:{'slug':post.id}}}><input type="submit" class="bttonPost" value="VER DATOS" /></Link></p>))}
        </div>
        <Footer />
    </div>
);

export async function getServerSideProps() {
    const req = await fetch('http://localhost/wordpress/wp-json/wp/v2/proyecto')
    const data = await req.json()
    return {      
        props: {
            posts: data
        },
    };
}

 export default listaProyectos;