import Head from 'next/head';
import Link from 'next/link';
import NavBar from './comps/NavBar';
import Footer from './comps/Footer';
import { useRouter } from 'next/router';

const proyDetails = ({details}) => {
    if(details=="N/A"){
        return(<p>No hay datos</p>);
    }
    return(
        <div>
            <Head>
                <title>Detalle de proyecto</title>
                <link rel="icon" href="http://www.classicgaming.cc/classics/pac-man/images/icons/pac-man.ico" />
            </Head>
            <NavBar />
            <br/><br/>
            <div className="divIniII">
                <h1>Detalles del proyecto</h1>
                <p>ID: {details.id}</p>
                <p>Nombre: {details.title['rendered']}</p>
                <p>Empresa: {details.EMPRESA}</p>
                <p>Url: {details.URL}</p>
                <p>Url-Proyecto: {details.link}</p>
                <p>Slug: {details.slug}</p>
                <p>Fecha: {details.FECHA}</p>
                <p>Tipo: {details.type}</p>
                <p>Estado: {details.status}</p>
            </div>
            <Footer/>
        </div>
    )
};

 export default proyDetails; 


 export async function getServerSideProps({query:{slug}}){
    if(slug!=null){
        const req = await fetch('http://localhost/wordpress/wp-json/wp/v2/proyecto/'+slug)
        const data = await req.json()
    return {
        props: {
            details: data},
        };
    }
    else{
        return("N/A");
    }
}