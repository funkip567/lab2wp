import Head from 'next/head';
import Link from 'next/link';
import NavBar from './comps/NavBar';
import Footer from './comps/Footer';
const contact = () =>{
    return(
        <div>
            <Head>
                <title>Contacto</title>
                <link rel="icon" href="http://www.classicgaming.cc/classics/pac-man/images/icons/pac-man.ico" />
            </Head>
            <NavBar/>
            <div className="divIniIII">
                <h1>Mis principales redes de contacto</h1>
                <p><img className="divIMGIniIcon" src="https://icones.pro/wp-content/uploads/2021/03/icone-gmail-logo-png-grise.png"/> Gmail: panchoponzoni@gmail.com</p>
                <p><img className="divIMGIniIcon" src="http://pngimg.com/uploads/linkedIn/linkedIn_PNG38.png"/> Linkedin: https://www.linkedin.com/in/francis-ponzoni-9a91871bb/</p>
                <p><img className="divIMGIniIcon" src="https://avatars.githubusercontent.com/u/10574326?v=4"/> EDteam: https://app.ed.team/@funkip567/</p>
                <p><img className="divIMGIniIcon" src="https://blog.codepen.io/wp-content/uploads/2012/06/Button-Fill-Black-Large.png"/> Codepen: https://codepen.io/francisco-ponzoni</p>
                <p><img className="divIMGIniIcon" src="https://pbs.twimg.com/media/Edi7CuKU0AAJItn.png"/> GitLab: https://gitlab.com/funkip567</p>
                <p><img className="divIMGIniIcon" src="https://www.freepnglogos.com/uploads/discord-logo-png/discord-logo-logodownload-download-logotipos-1.png"/> Discord: https://discord.com/channels/funkip567#5204</p>
                <p><img className="divIMGIniIcon" src="https://upload.wikimedia.org/wikipedia/commons/0/08/Pinterest-logo.png"/> Pinterest: https://www.pinterest.com/panchoponzoni/</p>
                <p><img className="divIMGIniIcon" src="http://assets.stickpng.com/thumbs/58af04446c252499281ae91f.png"/> SoundCloud: https://soundcloud.com/francisco-ponzoni</p>
            </div>
            <Footer/>
        </div>
    );
}
export default contact;